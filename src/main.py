from customtkinter import *
import os
from tkinter import filedialog
import configparser

class APP:
    def __init__(self):
        # Creating customtkinter window - 700x500, Lobster, dark mode • setting variables
        config = configparser.ConfigParser()
        self.config = config
        self.config.read("ini_conf_files/settings.ini")
        colors = self.config['COLORS']
        app_mode = self.config['APPEARANCE_MODE']
        text = self.config['TEXT'] 
        app = CTk()
        self.app = app
        self.app.geometry("700x500")
        self.app.title("Lobster")
        self.app.iconbitmap("images/lobster-logo.ico")
        self.color = ["#FFFFFF", "#4287f5", "#e61920", "#19e619", "#1d1e1e", "#dfe619"]
        self.color_text = ["white", "blue", "red", "green", "black", "yellow"]
        appearance_mode = "dark"
        self.appearance_mode = app_mode['app_mode']
        self.button_color = "#1d1e1e"
        set_appearance_mode(self.appearance_mode)
        if colors['button'] [0] == "#":
            self.button_color = colors ['button']
        else:
            self.button_color = "#" + colors['button']
        if colors['text'] [0] == "#":
            self.text_color = colors ['text']
        else:
            self.text_color = "#" + colors ['text']
        self.text_size = text['size']
        self.button_color_text = self.color_text[self.color.index(self.button_color)]
        self.text_color_text = self.color_text[self.color.index(self.text_color)]

    def save_file(self):
         # Function which saves file after editing
         if self.path_to_file == "":
             self.path_to_file = filedialog.askopenfilename()
             with open(self.path_to_file, "w") as f:
                f.write(self.text.get("0.0", "end"))
                f.close()
         else:
             os.system("del " + self.path_to_file)
             with open(self.path_to_file, "w") as f:
                f.write(self.text.get("0.0", "end"))
                f.close()
    def open_file(self):
        # Function which opens explorer to choose the current file and opens it
        self.text.delete("0.0", "end")
        path_to_file = filedialog.askopenfilename()
        self.path_to_file = path_to_file
        with open(self.path_to_file, "r") as f:
            self.text.insert("0.0", f.read())
            f.close()
    def app_mode_set(self, choice):
        self.config.set('APPEARANCE_MODE', 'app_mode', choice)
        with open('ini_conf_files/settings.ini', 'w') as configfile:
            self.config.write(configfile)
            configfile.close()
    def button_color_set(self, choice):
        choice = self.color[self.color_text.index(choice)]
        self.config.set("COLORS", "button", choice)
        with open("ini_conf_files/settings.ini", "w") as configfile:
            self.config.write(configfile)
            configfile.close()
    def text_color_set(self, choice):
        choice = self.color[self.color_text.index(choice)]
        self.config.set("COLORS", "text", choice)
        with open("ini_conf_files/settings.ini", "w") as configfile:
            self.config.write(configfile)
            configfile.close()
    def settings_window(self):
        # Creating new window with settings for colors, themes and text font
        set_win = CTkToplevel(self.app)
        self.set_win = set_win
        self.set_win.title("Settings • Lobster")
        self.set_win.geometry("500x500")
        self.set_win.focus()
        CTkLabel(self.set_win, text="Colors and Theme", font=CTkFont(size=25)).place(x=10, y=10)
        CTkLabel(self.set_win, text="Appearance mode:").place(x=10, y=50)
        app_mode_cb = CTkComboBox(self.set_win, values=["dark", "light"], command=self.app_mode_set)
        self.app_mode_cb = app_mode_cb
        self.app_mode_cb.set(self.appearance_mode)
        self.app_mode_cb.place(x=130, y=50)
        CTkLabel(self.set_win, text="Button color:").place(x=10, y=80)
        button_color_cb = CTkComboBox(self.set_win, values=["white", "blue", "red", "green", "black", "yellow"], command=self.button_color_set)
        self.button_color_cb = button_color_cb
        self.button_color_cb.set(self.button_color_text)
        self.button_color_cb.place(x=130, y=80)
        CTkLabel(self.set_win, text="Text color:").place(x=10, y=110)
        text_color_cb = CTkComboBox(self.set_win, values=["white", "blue", "red", "green", "black", "yellow"], command=self.text_color_set)
        self.text_color_cb = text_color_cb
        self.text_color_cb.set(self.text_color_text)
        self.text_color_cb.place(x=130, y=110)
    def layout_main_window(self):
        # Creating layout of main window
        text = CTkTextbox(self.app, activate_scrollbars=True, width=700, height=460,  font=CTkFont(size=int(self.text_size)), text_color=self.text_color)
        self.text = text
        self.text.place(x=0, y=40)
        menu_frame = CTkFrame(self.app, width=700, height=40)
        self.menu_frame = menu_frame
        self.menu_frame.place(y=0, x=0)
        open_ = CTkButton(self.menu_frame, text="Open", command=self.open_file, width=60, fg_color=self.button_color, border_color=self.button_color)
        self.open_ = open_
        self.open_.place(x=90, y=7.5)
        save = CTkButton(self.menu_frame, text="Save", command=self.save_file, width=60, fg_color=self.button_color, border_color=self.button_color)
        self.save = save
        self.save.place(x=10, y=7.5)
        settings = CTkButton(self.menu_frame, text="Settings", width=60, command=self.settings_window, fg_color=self.button_color, border_color=self.button_color)
        self.settings = settings
        self.settings.place(x=170, y=7.5)
    def run(self):
        self.app.mainloop()


program = APP()
program.layout_main_window()

program.run()
